# README

## Development environment

### Prerequisites

1. Opam version 2.x.x (https://opam.ocaml.org/)

2. Create a local Opam switch in the project directory and set the shell environment:
   `opam switch create ./`
   `eval $(opam config env)`

3. merlin, ocp-indent, dune, utop, ounit and core:
   `opam install merlin ocp-indent dune utop ounit core`

4. Why3 1.2.0 :
   `opam install why3.1.2.0`

### IDE

VSCode with the extensions reason-vscode and ocaml-debugger is a good choice. Any text editor could of course also be used.

## Build, run and test

To build: `dune build bin/main.exe`. The binary is created in `_build/default/bin`.

To build *and* run the main executable: `dune exec bin/main.exe`

### Debugging in VSCode

1. Install earlybird (in the project directory):
    `opam install earlybird`
2. Compile to *bytecode* with *dev profile*:
    `dune build --profile=dev bin/main.bc`
3. Set a breakpoint somewhere in the code.
4. Open VS Code debug panel and launch "OCaml Debug" configuration.

