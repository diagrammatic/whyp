open Why3
open Format 

open Whyconf

let files = Queue.create ()
let add_file x = 
  Queue.add x files

let opt_config = ref None
let opt_extra = ref []
let opt_parser = ref None

let opt_provers = ref []
let add_opt_prover x =
  opt_provers := x::!opt_provers

let opt_loadpath = ref []

let opt_timelimit = ref None
let opt_memlimit = ref None

let opt_trans = ref []
let add_opt_trans x = opt_trans := x::!opt_trans

let opt_j = ref None

let exit_code = ref 0

type runnable_prover =
  { rp_command : string;
    rp_driver : Driver.driver;
    rp_prover : Whyconf.prover }

let option_list = Arg.align [
    "-", Arg.Unit (fun () -> add_file "-"), " Read the input file from stdin";
    "-C", Arg.String (fun s -> opt_config := Some s), "<file> Read configuration from <file>";
    "--config", Arg.String (fun s -> opt_config := Some s), " same as -C";
    "--extra-config", Arg.String (fun s -> opt_extra := !opt_extra @ [s]),  "<file> Read additional configuration from <file>";
    "-L", Arg.String (fun s -> opt_loadpath := s :: !opt_loadpath), "<dir> Add <dir> to the library search path";
    "--library", Arg.String (fun s -> opt_loadpath := s :: !opt_loadpath), " same as -L";
    "-I", Arg.String (fun s -> opt_loadpath := s :: !opt_loadpath), " same as -L (obsolete)";
    "-P", Arg.String add_opt_prover, "<prover> Try to prove the goals with this prover";
    "--prover", Arg.String add_opt_prover, " same as -P";
    "-F", Arg.String (fun s -> opt_parser := Some s), "<format> Select input format (default: \"why\")";
    "--format", Arg.String (fun s -> opt_parser := Some s), " same as -F";
    "-a", Arg.String add_opt_trans, "<transformation> apply a transformation to every task";
    "--apply-transform", Arg.String add_opt_trans, " same as -a";
    "-t", Arg.Int (fun i -> opt_timelimit := Some i), "<sec> Set the prover's time limit (default=10, no limit=0)";
    "--timelimit", Arg.Int (fun i -> opt_timelimit := Some i), " same as -t";
    "-m", Arg.Int (fun i -> opt_memlimit := Some i), "<MiB> Set the prover's memory limit (default: no limit)";
    "--memlimit", Arg.Int (fun i -> opt_timelimit := Some i), " same as -m";
    "-j", Arg.Int (fun i -> opt_j := Some i), "<int> Set the number of worker to use (default:1)";
    Debug.Args.desc_debug_list;
    Debug.Args.desc_debug_all;
    Debug.Args.desc_debug;
  ]

let readAndParseFile config s = 
  try (
    let main = Whyconf.get_main config in
    opt_loadpath := List.rev_append !opt_loadpath (Whyconf.loadpath main);
    if !opt_timelimit = None then opt_timelimit := Some (Whyconf.timelimit main);
    if !opt_memlimit  = None then opt_memlimit  := Some (Whyconf.memlimit main);
    let timelimit = match !opt_timelimit with
      | None -> 10
      | Some i when i <= 0 -> 0
      | Some i -> i in
    let memlimit = match !opt_memlimit with
      | None -> 0
      | Some i when i <= 0 -> 0
      | Some i -> i in
    let reslimit = { Call_provers.limit_time = timelimit;
                     Call_provers.limit_mem = memlimit;
                     Call_provers.limit_steps=1 } in 
    let env = Env.create_env !opt_loadpath in
    let format = !opt_parser in
    let theories = match s with 
      | "-" -> Env.read_channel Env.base_language ?format env "stdin" stdin
      | fname -> Env.read_file Env.base_language ?format env fname
    in
    let provers : Whyconf.config_prover Whyconf.Mprover.t = Whyconf.get_provers config  in
    let shortc_provers : Whyconf.prover Wstdlib.Mstr.t = Whyconf.get_prover_shortcuts config in
    let shortc_given_provers =  Wstdlib.Mstr.filter
        (fun t _ ->
           (List.mem t (!opt_provers))) (shortc_provers) in
    (* find the prover given by the user *)
    let given_provers = 
      List.filter (fun a -> (List.mem a.prover (Wstdlib.Mstr.values shortc_given_provers))) 
        (Whyconf.Mprover.values provers)
    in

    (* If the transformation rules from the command line is empty, register the default transformation rules *)
    if (List.length (!opt_trans) = 0 ) then
      List.iter add_opt_trans Lib.Transform.default_transforms; 

    let lookup acc t =
      (try Trans.singleton (Trans.lookup_transform t env) with
         Trans.UnknownTrans a ->  Trans.lookup_transform_l t env) :: acc in 

    let trans = List.fold_left lookup [] !opt_trans in
    let apply tasks tr =
      List.rev (List.fold_left (fun acc task ->
          List.rev_append (Trans.apply tr task) acc) [] tasks)
    in

    (* task and goal construction *)
    let tasks th : Task.task list = 
      List.fold_left apply (Task.split_theory th None None) trans 
    in

    let driver pr : Driver.driver = 
      try Whyconf.load_driver main env pr.Whyconf.driver [] 
      with e -> eprintf "Failed to load driver for prover: %a@." Exn_printer.exn_printer e; exit 1 in

    (* try each each by provers and if it returns valid, break *)
    let unproved_counter = ref (0) in 
    Wstdlib.Mstr.iter (fun _ th -> 
      printf "%a" Pretty.print_theory th ;
      List.iter (fun task -> 
        let goalid = (Task.task_goal task).Decl.pr_name.Ident.id_string in
        let sub_tasks = Lib.Transform.apply_transform_on_task env task (Lib.Transform.get_transforms goalid) in 
        let proved = ref (List.length sub_tasks == 0) in (* if there are zero subtasks, the theorem was discharged already by transformation *)
        List.iter (fun sub_task ->
          List.iter ( fun prover -> 
            let res = 
              if (!proved == false ) 
              then (Call_provers.wait_on_call (Driver.prove_task ~limit:reslimit ~command:prover.Whyconf.command (driver prover) sub_task)).Call_provers.pr_answer 
              else Call_provers.Valid in
            proved := !proved || res == Call_provers.Valid)
          given_provers;
          if !proved == false then (
            unproved_counter := !unproved_counter + 1;
            printf "Not proved: %s\n" goalid 
          ))
        sub_tasks)
      (tasks th))
    theories;
    printf "Number of unproved tasks: %d \n" !unproved_counter;
    exit_code := if !unproved_counter == 0 then 0 else 1;
  )
  with Why3.Loc.Located (pos, ex) -> 
    let (f,l,b,e) = Loc.get pos in
    (Format.printf "File \"%s\", line %d, characters %d-%d\n" f l b e);
    match ex with 
    | Parser.Error -> Format.printf "Parser Error\n"; exit_code := 1
    | Lib.Translate.NotValid msg -> Format.printf "Invalid Partition Diagram.\n%s\n" msg; exit_code := 1
    | _ ->  Format.printf "%a" Exn_printer.exn_printer ex; raise ex   

let usage_msg = sprintf
    "Usage: %s [options] [[file|-]"
    (Filename.basename Sys.argv.(0))

let () =
  Whyparsing.Parsing.register_format;
  Arg.parse option_list add_file usage_msg;
  let config = List.fold_left merge_config (Whyconf.read_config !opt_config) !opt_extra in
  Queue.iter (fun file -> readAndParseFile config file) files;
  exit !exit_code
