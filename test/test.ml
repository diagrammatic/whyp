open OUnit

let _ = 
  let _ = run_test_tt_main Test_parsing.suite in 
  let _ = run_test_tt_main Test_translation.suite in 
  let _ = run_test_tt_main Test_reduce.suite in
  let _ = run_test_tt_main Test_ex.suite in
  ()
