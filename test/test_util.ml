open Whyparsing
open Why3
open OUnit
open Lib

(* functions for setting all positions to dummy location *)

let ident_rmloc_deep id =  
  let open Why3.Ptree in
  { 
    id_str = id.id_str;
    id_ats = id.id_ats;
    id_loc = Loc.dummy_position
  }

let rec qualid_rmloc_deep q = 
  let open Why3.Ptree in
  match q with 
  | Qident id -> Qident (ident_rmloc_deep id)
  | Qdot (q1,id) -> Qdot ((qualid_rmloc_deep q1),(ident_rmloc_deep id))

let rec pty_rmloc_deep t= 
  let open Why3.Ptree in
  match t with 
  | PTtyvar id -> PTtyvar (ident_rmloc_deep id)
  | PTtyapp (q,tl) ->  PTtyapp (qualid_rmloc_deep q, (List.map pty_rmloc_deep tl))
  | PTtuple tl -> PTtuple (List.map pty_rmloc_deep tl)
  | PTref   tl -> PTref (List.map pty_rmloc_deep tl)
  | PTarrow (t1,t2)  -> PTarrow ((pty_rmloc_deep t1),(pty_rmloc_deep t2))
  | PTscope (q,t1) -> PTscope ((qualid_rmloc_deep q),(pty_rmloc_deep t1))
  | PTparen t1 -> PTparen (pty_rmloc_deep t1)
  | PTpure  t1 -> PTpure (pty_rmloc_deep t1)

let rec binder_rmloc_deep (a,io,g,t) = 
  (Loc.dummy_position,io,g,t)

let rec param_rmloc_deep (a,io,g,t) = 
  (Loc.dummy_position,io,g,t)

let attr_rmloc_deep a =
  let open Why3.Ptree in
  match a with
  | ATpos _ -> ATpos Loc.dummy_position
  | _ -> a

let rec term_rmloc_deep t =
  let open Why3.Ptree in
  let desc' = (match t.term_desc with 
      | Tident q -> Tident (qualid_rmloc_deep q)
      | Tasref q -> Tasref (qualid_rmloc_deep q)
      | Tidapp (q,tl) -> Tidapp ((qualid_rmloc_deep q),(List.map term_rmloc_deep tl))
      | Tapply (t1,t2) -> Tapply ((term_rmloc_deep t1),(term_rmloc_deep t2))
      | Tinfix (t1,id,t2) -> Tinfix ((term_rmloc_deep t1),(ident_rmloc_deep id),(term_rmloc_deep t2))
      | Tinnfix (t1,id,t2) -> Tinnfix ((term_rmloc_deep t1),(ident_rmloc_deep id),(term_rmloc_deep t2))
      | Tbinop (t1,dbo,t2) -> Tbinop ((term_rmloc_deep t1),dbo,(term_rmloc_deep t2))
      | Tbinnop (t1,dbo,t2) -> Tbinnop ((term_rmloc_deep t1),dbo,(term_rmloc_deep t2))
      | Tnot t1 -> Tnot (term_rmloc_deep t1)
      | Tif (t1,t2,t3) -> Tif ((term_rmloc_deep t1),(term_rmloc_deep t2),(term_rmloc_deep t3))
      | Tquant (dqnt, bl, tll, t1) -> 
        Tquant (dqnt,
                (List.map binder_rmloc_deep bl),
                (List.map (List.map (term_rmloc_deep)) tll),
                (term_rmloc_deep t1))
      | Tattr (a,t1) -> Tattr ((attr_rmloc_deep a),(term_rmloc_deep t1))
      | Tlet (id,t1,t2) ->  Tlet ((ident_rmloc_deep id),(term_rmloc_deep t1),(term_rmloc_deep t2))
      | Tcase (t1,tpl) ->  Tcase ((term_rmloc_deep t1), tpl) (* TODO: patterns *)
      | Tcast (t1,pt) -> Tcast ((term_rmloc_deep t1),(pty_rmloc_deep pt))
      | Ttuple tl -> Ttuple (List.map term_rmloc_deep tl)
      | Trecord qtl -> Trecord (List.map (fun (q,t) -> ((qualid_rmloc_deep q),(term_rmloc_deep t))) qtl)
      | Tupdate (t1,qtl) -> Tupdate ((term_rmloc_deep t1),(List.map (fun (q,t) -> ((qualid_rmloc_deep q),(term_rmloc_deep t))) qtl))
      | Tscope (q,t1)  -> Tscope ((qualid_rmloc_deep q),(term_rmloc_deep t1))
      | Tat  (t1,id) -> Tat ((term_rmloc_deep t1),(ident_rmloc_deep id))
      | _ -> t.term_desc
    ) in { term_desc=desc'; term_loc=Loc.dummy_position; }

let rec parsed_pd_diag_rmloc pdd =
  let open Lib.Partition in
  match pdd with
  | NONEMPTY_CELL (t,oc) -> NONEMPTY_CELL (term_rmloc_deep t, Opt.map term_rmloc_deep oc)
  | SeqL (t,oc) -> SeqL (term_rmloc_deep t, Opt.map term_rmloc_deep oc)
  | SeqR (t,oc) -> SeqR (term_rmloc_deep t, Opt.map term_rmloc_deep oc)
  | EMPTY_Seq oc  ->  EMPTY_Seq (Opt.map term_rmloc_deep oc)
  | SeqLR (t1,t2,oc) -> SeqLR (term_rmloc_deep t1, term_rmloc_deep t2, Opt.map term_rmloc_deep oc)

let rec parsed_pd_endt endt = 
  let open Lib.Partition in
  match endt with
  | Some t ->  Some (term_rmloc_deep t)
  | None -> None

let rec parsed_pd_rmloc (first, diags, last) =
  let open Lib.Partition in
  let first' = (match first with
    | Some t -> Some (term_rmloc_deep t) 
    | None -> None) in
  let diags' = List.map parsed_pd_diag_rmloc diags in
  let last' = (match last with
    | Some t -> Some (term_rmloc_deep t) 
    | None -> None) in
  (first',diags',last')

let parse_pd s =
  let pd = Whyparser.prt_eof Whylexer.token (Lexing.from_string s) in
  parsed_pd_rmloc pd

let parse_term s =
  let pt = Whyparser.term_eof Whylexer.token (Lexing.from_string s) in
  term_rmloc_deep pt

let load_and_prove fname tryprovers polarity timelimit loadPaths =
  try (
    let config = Whyconf.read_config None in
    let main = Whyconf.get_main config in
    let loadpath = (Whyconf.loadpath main) @ loadPaths in
    let memlimit = Whyconf.memlimit main in
    let reslimit = { Call_provers.limit_time = timelimit;
                     Call_provers.limit_mem = memlimit;
                     Call_provers.limit_steps = 1 } in 
    let env = Env.create_env loadpath in
    let theories = Env.read_file Env.base_language env fname in
    let provers = Whyconf.get_provers config in
    let shortc_provers = Whyconf.get_prover_shortcuts config in
    Wstdlib.Mstr.iter (fun _ th ->     
      let tasks = Task.split_theory th None None in
      List.iter (fun task -> 
        let goalid = (Task.task_goal task).Decl.pr_name.Ident.id_string in
        let sub_tasks = Transform.apply_transform_on_task env task (Transform.get_transforms goalid) in 
        let proved = ref (List.length sub_tasks == 0) in
        List.iter (fun sub_task ->
          List.iter (fun prover -> 
            if not !proved then (
              if not (Wstdlib.Mstr.mem prover shortc_provers) then assert_failure ("unknown prover: "^prover);
              let pr = Wstdlib.Mstr.find prover shortc_provers in 
              let pc = List.find (fun a -> a.Whyconf.prover = pr) (Whyconf.Mprover.values provers) in
              let driver = Whyconf.load_driver main env pc.Whyconf.driver [] in
              let res = Call_provers.wait_on_call (Driver.prove_task ~limit:reslimit ~command:pc.Whyconf.command driver sub_task) in
              if (res.Call_provers.pr_answer == Call_provers.Valid) then proved := true;)
          ) tryprovers
        ) sub_tasks;
        if (polarity && not !proved) then assert_failure (goalid ^ ": not proved, but expected to be")
        else if (not polarity && !proved) then assert_failure (goalid ^ ": proved, but expected not to be"))
      tasks
      )
      theories
  )

  with Why3.Loc.Located (pos, ex) -> 
    let (f,l,b,e) = Loc.get pos in
    let locs = Format.sprintf "File \"%s\", line %d, characters %d-%d\n" f l b e in
    match ex with 
    | Parser.Error -> assert_failure (locs ^ ": parser error")
    | Lib.Translate.NotValid msg ->  assert_failure (locs ^ ": invalid partition diagram (" ^ msg ^ ")")
    | _ -> print_endline locs; raise ex 

let () =
  Whyparsing.Parsing.register_format;
