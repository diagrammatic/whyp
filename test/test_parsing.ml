open OUnit
open Lib.Partition  
open Test_util 

let assert_parsed s e = 
  "case "^s >:: (fun _ -> assert_equal (parse_pd s) e)

let suite = 
  let pt = parse_term in
  "parsing" >::: [
    assert_parsed "[1]"               (None, [NONEMPTY_CELL (pt "1", None)], None);
    assert_parsed "[a]"               (None, [NONEMPTY_CELL (pt "a", None)], None);
    assert_parsed "[1 ... |2]"        (None, [SeqL (pt "1", None)], Some (pt "2"));
    assert_parsed "[1 ... 2]"         (None, [SeqLR (pt "1", pt "2", None)], None);
    assert_parsed "[1| ... 2]"        (Some (pt "1"), [SeqR (pt "2", None)], None);
    assert_parsed "[1| ... |2]"       (Some (pt "1"), [EMPTY_Seq None], Some (pt "2"));
    assert_parsed "[4|5]"             (Some (pt "4"), [NONEMPTY_CELL (pt "5", None)], None);
    assert_parsed "[i+1| ... j]"      (Some (pt "i+1"), [SeqR (pt "j", None)], None);
    assert_parsed "[i-1| ... j]"      (Some (pt "i-1"), [SeqR (pt "j", None)], None);
    assert_parsed "[1| ... 2| ... 3]" (Some (pt "1"), [SeqR (pt "2", None); SeqR (pt "3", None)], None);
    assert_parsed "[a #(C)]"            (None, [NONEMPTY_CELL (pt "a", Some (pt "C"))], None);
    assert_parsed "[a#(C)]"             (None, [NONEMPTY_CELL (pt "a", Some (pt "C"))], None);
    assert_parsed "[1 ...#(C) |2]"      (None, [SeqL (pt "1", Some (pt "C"))], Some (pt "2"));
    assert_parsed "[1| ... #(C) |2]"    (Some (pt "1"), [EMPTY_Seq (Some (pt "C"))], Some (pt "2"));
  ]
