module PartitionDiagrams
  use int.Int
  use partition.PartitionInt
  use option.Option

  (* true partition diagrams *)

  goal a: [1]

  goal aa: [ 1| ... 2 ]
  goal ab: [ 1| ... |2 ]
  goal ac: [ 1 ... |2 ]
  goal ad: [ 1 ... 2 ]

  goal ba: [ 4 ... |6| ... 7 ]

  (* basic correspondences *)

  constant a: int
  constant b: int
  constant k: int

  goal basic1: [ a| ... b ] <-> a <= b
  goal basic2: [ a ... |b ] <-> a-1 <= b-1
  goal basic3: [ a| ... |b ] <-> a <= b-1
  goal basic4: [ a ... b ] <-> a-1 <= b

  goal composite1: [a ... |k| ... b] <-> a <= k <= b

  (* relations between partition diagrams *)

  goal singleton: [ a ] <-> [ a-1| ... a ]

  goal craftprog_81_1: [ a ... |k| ... b ] <->  [ a ... |k ... k| ... b ]

  goal craftprog_81_2: [ a ... |k ... k| ... b ] <-> [ a-1| ... k-1| ... k| ... b ]
  
end

module ArrayPartition
  use int.Int
  use array.Array
  use partition.PartitionInt
  use option.Option

  constant a: array int
  constant m: int
  constant k: int

  goal array_nonempty_1: [0 ... |k| ... |length a] -> length a > 0
  goal array_nonempty_2: [0| ... |k| ... length a] -> length a > 0
  goal array_nonempty_3: [0 ... |k|m| ... length a] -> length a > 0
end