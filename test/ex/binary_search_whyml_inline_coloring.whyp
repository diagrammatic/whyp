module Binary_search 

    use int.Int
    use array.Array
    use int.EuclideanDivision
    use ref.Ref
    use partition.PartitionInt
    use option.Option

    exception Not_Found 
    
    color col = R | B

    legend leg_binary_search (a:array int)(x:int) of col =
        i: [i#(R)] -> a[i] <> x ;
        i: [i#(B)] -> a[i] = x

    predicate sorted(a:array int)=
       forall i j:int . [0 ... |i|j| ... |length a] -> a[i] <= a[j]

    let binary_search (a: array int)(v: int)  : int 
        requires { sorted a } 
        ensures  { [0 ... |result| ... |length a] }
        ensures  { leg_binary_search a v (maps (i:int) [result #(B)]) }
        raises   { Not_Found -> leg_binary_search a v (maps (i:int) [0 ...#(R) |length a]) } 
    =
        let l = ref 0 in
        let u = ref (length a - 1) in
        while !l <= !u do 
            invariant { sorted a }
            invariant { [0 ... | !l] && [!u ... |(length a)-1] }
            invariant { leg_binary_search a v (maps (i:int) [0 ...#(R) | !l  ... !u | ... #(R) |length a]) } 
            variant { !u - !l } 

            let m = !l + div (!u - !l) 2 in  
            if a[m] < v then 
                l := m + 1 
            else if a[m] > v then
                u := m - 1
            else 
                return m 
        done;

        raise Not_Found 

end