open OUnit
open Test_util 

let get_files_in_dir dir = 
  let files = Array.to_list (Sys.readdir dir) in
  let whypFiles = List.filter (fun fname -> Str.string_match (Str.regexp ".*\\.whyp$") fname 0) files in
  List.map (fun filename -> dir^"/"^filename) whypFiles


let provers = ["z3"; "cvc4"]

let suite = 
  "examples" >::: 

  (* expected ALWAYS to be proved *)
  List.map 
    (fun path -> 
       (path >:: (fun _ -> 
       load_and_prove (path) provers true 30 ["../theories"] )))
    (get_files_in_dir "ex")

  (* expected NEVER be proved -*)
  @ List.map 
    (fun path -> 
       (path >:: (fun _ -> load_and_prove (path) provers false 5 ["../theories"] )))
    (get_files_in_dir "nex")
