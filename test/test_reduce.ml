open OUnit
open Lib.Reduce
open Lib.Partition
open Test_util 

let assert_reduced pd e = 
  "case "^e >::  (fun _ -> assert_equal (norm_to_ptree pd) (parse_term e))

let suite = 
  "reduction" >::: [
    assert_reduced  (Last (parse_term "1", None, parse_term "2"))                                 "1<=2";
    assert_reduced  (Part (parse_term "1", None, Last (parse_term "2",None,parse_term "3")))      "1<=2<=3";
  ]

