open OUnit
open Lib.Translate 
open Lib.Partition
open Lib.Ptree_util
open Test_util 

let assert_trs s e = 
  "case "^s >:: (fun _ -> 
    let pd = translate (parse_pd s) in
    assert_equal pd e)

let assert_trs_raises s e = 
  "case "^s >:: (fun _ -> assert_raises e (fun () -> translate (parse_pd s)))

let suite = 
  let pt = parse_term in
  let pre t = term_noloc (Tidapp (qid_pred, [t])) in 
  "translation" >::: [
    assert_trs "[3]"                          (Last (pre (pt "3"), None, pt "3"));
    assert_trs "[a]"                          (Last (pre (pt "a"), None, pt "a")); 
    assert_trs "[4 ... 5|6 ... 7]"            (Part (pre (pt "4"), None, Part (pt "5", None, Last (pre (pt "6"), None, pt "7")))); 
    assert_trs "[1 ... 2]"                    (Last (pre (pt "1"), None, pt "2")); 
    assert_trs "[4|5|6]"                      (Part (pt "4", None, Part (pre (pt "5"), None, Last (pt "5", None, pre (pt "6"))))); 
    assert_trs "[4|5]"                        (Part (pt "4", None, Last (pre (pt "5"), None, pt "5")));
    assert_trs "[5| ... |7]"                  (Last (pt "5", None, pre (pt "7"))); 
    assert_trs "[5| ... 7]"                   (Last (pt "5", None, pt "7")); 
    assert_trs "[5|7 ... 9|10 ... 13]"        (Part (pt "5", None, Part (pre (pt "7"), None, Part (pt "9", None, Last (pre (pt "10"), None, pt "13"))))); 
    assert_trs "[1-b ... (2+a)]"              (Last (pre (pt "1-b"), None, pt "2+a")); 
    assert_trs "[3#(R)]"                        (Last (pre (pt "3"), Some (pt "R"), pt "3"));
    assert_trs "[4|5#(R)]"                      (Part (pt "4", None, Last (pre (pt "5"), Some (pt "R"), pt "5"))); 
    assert_trs "[1 ...#(R) 2]"                  (Last (pre (pt "1"), Some (pt "R"), pt "2")); 
    assert_trs "[5|7 ...#(R) 9|10 ... 13]"      (Part (pt "5", None, Part (pre (pt "7"), Some (pt "R"), Part (pt "9", None, Last (pre (pt "10"), None, pt "13"))))); 
    assert_trs "[-1| ...#(R) m| ... |n]"        (Part (pt "-1", Some (pt "R"), Last (pt "m", None, pre (pt "n")))); 
    assert_trs "[-1| ...#(R) |n]"               (Last (pt "-1", Some(pt "R"), pre (pt "n")));
    assert_trs "[-1| ... m|k| ...#(R) |n]"      (Part (pt "-1", None, Part (pt "m", None, Part(pre (pt "k"), None, Last (pt "k", Some (pt "R"), pre (pt "n")))))); 

    assert_trs_raises "[1-b ... ]"            (NotValid "Missing bound");
    assert_trs_raises "[1| ... ]"             (NotValid "Missing bound");
    assert_trs_raises "[ ... 2]"              (NotValid "Missing bound");
    assert_trs_raises "[1 ... | ... 2]"       (NotValid "Missing bound"); 
  ]
