%{
    open Why3
    open Ptree
    
    let floc s e = Loc.extract (s,e)
    let mk_term d s e = { term_desc = d; term_loc = floc s e }
    let mk_id id s e = { id_str = id; id_ats = []; id_loc = floc s e }

  (*  let prt_to_ptree prt = 
        Lib.Reduce.norm_to_ptree (Lib.Translate.translate prt) *)
    let mk_term_noloc d = { term_desc = d; term_loc = Loc.dummy_position }
    let mk_id_noloc t = {id_str = t; id_ats = []; id_loc = Loc.dummy_position }
    let mk_Tident id = Ptree.Tident (Ptree.Qident id)

    let append_elem l el = l@[el]

    let ty_int = mk_id_noloc "int"
    let cl_ident = mk_id_noloc "~col" 

    let partitioningPred_qid = Qdot (Qident (mk_id_noloc "M"), mk_id_noloc "partitioning")  
    let coloring_fun_qid = Qdot (Qident (mk_id_noloc "M"), mk_id_noloc "mapping")   

    let mk_Ttyapp ty = PTtyapp (Qident ty,[]) 

    let prt_to_ptree prt = 
       mk_term_noloc (Tidapp (partitioningPred_qid, [Lib.Reduce.norm_to_partitionTyped_ptree (Lib.Translate.translate prt)] ))

    let get_id_of_param (_,id,_,_) = Opt.get id

    let mk_binder loc ident_opt ghost pty_opt = (loc, ident_opt, ghost, pty_opt)

    let mk_term_from_coloring (quant_vars,cl_term,after_imlp,pos_s,pos_e) = 
        mk_term (Tquant (Dterm.DTforall, 
                        (List.map 
                            (fun var_id -> 
                                 mk_binder var_id.id_loc (Some var_id) false (Some (mk_Ttyapp ty_int) )) 
                            quant_vars) , 
                        [] ,
                        mk_term (Tbinop (cl_term, Dterm.DTimplies, after_imlp)) pos_s pos_e ))
        pos_s 
        pos_e


    let last_param_pos_plus_one pos id_str = 
        let (_,l,s,e) = Loc.get pos in 
            Loc.user_position id_str l (e+1) (e+1+(String.length id_str)) 

    let mk_color_type cl state_params = 
      let pos_last_param = 
            match state_params with [] -> Loc.dummy_position 
                                | _ -> let (pos,_,_,_) = List.nth state_params ((List.length state_params)-1) in  pos  in 
        let cl_pos = last_param_pos_plus_one pos_last_param cl_ident.id_str in 
    (*  let seqt = List.fold_right 
                    (fun (_,_,_,t1) t2 -> 
                        PTarrow (t1,t2)) 
                    state_params 
                    (PTarrow (mk_Ttyapp ty_int,cl)) in
                    
            (cl_pos,Some cl_ident,false,seqt) *)
         (cl_pos,Some cl_ident,false,(PTarrow (mk_Ttyapp ty_int, PTtyapp (Qident (mk_id_noloc "option"), [cl] ))))  (* int -> option col *)

    let mk_anon_color_dt dt_type_id s e =  
        mk_id ("~MYCOLOR_" ^ dt_type_id.id_str) s e

    let mk_term_of_legends legends params s e = 
        List.map (fun (bound_vars,legend_antecedent,legend_consequent) -> 
            let state_var_ids = 
                List.map 
                    (fun param -> 
                        get_id_of_param param) params in
            let coloredpartition_term = Lib.Reduce.coloringlegendtoPtree (legend_antecedent, state_var_ids, cl_ident) in 
            mk_term_from_coloring (bound_vars, coloredpartition_term, legend_consequent, s, e)) legends 

    let mk_legendcalltermfromident optlegendcall s e =
        match optlegendcall with 
            | Some v -> let (leg_name,leg_param) = v in 
                let term_from_ident_list =
                    List.map 
                        (fun vid -> 
                            (mk_term vid s e)) 
                            (append_elem leg_param (mk_Tident cl_ident)) in

                    List.fold_left 
                        (fun v1 v2 -> 
                            Ptree.Tapply ((mk_term v1 s e),v2))
                        (mk_Tident leg_name)
                        term_from_ident_list

            | None -> Ttrue

    let mk_conjunctlegendterms legend_term_list legendcall_tid s e =
        List.fold_right 
            (fun a b -> 
                (mk_term (Tbinop (a ,
                    Dterm.DTand, b)) s e)) 
            legend_term_list
            (mk_term legendcall_tid s e)

    let mk_int_to_col_ty state_ty_list s e = PTarrow (mk_Ttyapp (mk_id "int" s e),  PTtyapp (Qident (mk_id "option" s e), [state_ty_list]))

    let mk_legend_rewite_rule legend_ident s e = Dmeta ((mk_id "rewrite_def" s e), [Mps (Qident legend_ident)])

    let mk_add_anon_to_algebraic_ty id def s e =   
        match def with 
            TDalgebraic dt -> 
                TDalgebraic (append_elem dt (Loc.dummy_position, mk_anon_color_dt id s e, [] ))
            | _ -> 
                raise Parser.Error

    let return_algebraic_Dtypes id def s e =   
        let l = match def with 
            TDalgebraic dt -> append_elem (List.map (fun (_,dt_id,_) -> dt_id.id_str ) dt) id.id_str 
            | _ -> 
                [] in l 

    (* color_type hashtable for storing col types' identifiers and its constructors *)
    let color_type = Hashtbl.create 1000 

    let add_type t_id elem = Hashtbl.add color_type elem t_id 

%}


%token LEGEND
%token COLOR
%token MAPS
%token COLSEP
%token PARTRANGE
%token PARTRANGE_COLSEP

%start <Lib.Partition.parsed_pd> prt_eof

%%

(*
color_val:
| uqualid                                                       { (mk_term (Ptree.Tident $1) $startpos $endpos) }
*)
color_val:
| LEFTPAR single_term RIGHTPAR                                  { $2 }

pd(C): 
| LEFTSQ single_term preceded(BAR,C) pd_tail(C)                 { let (d,e) = $4 in ((Some $2),$3::d,e) }
| preceded(LEFTSQ,C) pd_tail(C)                                 { let (d,e) = $2 in (None, ($1::d), e) }

pd_tail(C):
| BAR t = single_term RIGHTSQ                                   { ([], Some t) }
| RIGHTSQ                                                       { ([], None) }
| preceded(BAR,C) pd_tail(C)                                    { let (p,r)=$2 in ($1::p,r) }

part:
| single_term PARTRANGE single_term                             { Lib.Partition.SeqLR ($1,$3,None) }
| single_term PARTRANGE                                         { Lib.Partition.SeqL ($1,None) }
| PARTRANGE single_term                                         { Lib.Partition.SeqR ($2,None) }
| PARTRANGE                                                     { Lib.Partition.EMPTY_Seq None } 
| single_term                                                   { Lib.Partition.NONEMPTY_CELL ($1,None) }

opt_color:
| preceded(COLSEP,color_val)?                                   { $1 }

opt_color_range:
| PARTRANGE_COLSEP color_val                                    { Some $2 }
| PARTRANGE preceded(COLSEP,color_val)?                         { $2 }

part_color:
| single_term opt_color_range single_term                       { Lib.Partition.SeqLR ($1, $3, $2) }
| single_term opt_color_range                                   { Lib.Partition.SeqL ($1, $2) }
| opt_color_range single_term                                   { Lib.Partition.SeqR ($2, $1) }
| opt_color_range                                               { Lib.Partition.EMPTY_Seq ($1) } 
| single_term opt_color                                         { Lib.Partition.NONEMPTY_CELL ($1, $2) }

comma_list1(X):
| xl = separated_nonempty_list(COMMA, X)                        { xl }


coloring:
| pd(part_color)                                                { Lib.Translate.translate $1 } 

(*
coloring:
| pd(part_color)                                                {  Lib.Reduce.make_coloredpartList $1 }

colorings:
| cl = comma_list1 (coloring)                                   { List.flatten cl }
*)
colorings:
| cl = comma_list1 (coloring)                                   { cl } 


(* parsing of a single partition diagram (for testing) *)
prt_eof:
| e = pd(part_color) EOF                                        { e }

part_term_op:
| l = pd(part) ; o = bin_op ; r = single_term                   { mk_term (Tbinop ((prt_to_ptree l), o, r)) $startpos $endpos }
| l = single_term ; o = bin_op ; r = pd(part)                   { mk_term (Tbinop (l, o, (prt_to_ptree r))) $startpos $endpos }
| l = pd(part) ; o = bin_op ; r = pd(part)                      { mk_term (Tbinop ((prt_to_ptree l), o, (prt_to_ptree r))) $startpos $endpos }
| r = pd(part) ; o = bin_op ; a = part_term_op                  { mk_term (Tbinop (prt_to_ptree r, o, a)) $startpos $endpos }
| r = single_term;  o = bin_op ;  a = part_term_op              { mk_term (Tbinop (r, o, a)) $startpos $endpos }

%public
term:
| pd(part)                                                      { prt_to_ptree $1 } 
| part_term_op                                                  { $1 } 
(*| MAPS coloring                                                 { mk_term_noloc (Tidapp ( coloring_fun_qid, [Lib.Translate.norm_pd_to_why3_dt $2]))  }   *) (* Lib.Reduce.coloringfunctionToPtree $2*)                                                                                                                        
| MAPS LEFTPAR LIDENT cast RIGHTPAR coloring
                                                                {   let bind_ident = 
                                                                    let of_id id = id.id_loc, binder_of_id id in
                                                                        (of_id (mk_id $3 $startpos $endpos))  in
                                                                
                                                                    let (l,i) = ((floc $startpos($3) $endpos($3)),bind_ident) in 
                                                                    let q_ident = (l, set_ref_opt l false (snd i), false, Some $4) in 
                                                                        
                                                                    mk_term (Tquant (Dterm.DTlambda, 
                                                                        [q_ident], 
                                                                        [],
                                                                        mk_term_noloc (Tidapp ( coloring_fun_qid,
                                                                            [Lib.Translate.norm_pd_to_why3_dt $6; (mk_term (Tident (Qident (mk_id $3 $startpos $endpos))) $startpos $endpos) ])
                                                                        ))) $startpos $endpos
                                                                                                                        
                                                                }
                                                                          

legend_term:
| lident_extra COLON colorings ARROW term                       { ($1,(Lib.Reduce.norm_to_list_terms2 $3),$5) } 

legend_terms:
| cl = semicol_list1(legend_term)                               { cl }

semicol_list1(X):
| xl = separated_list(SEMICOLON, X)                             { xl }

lident_extra:                                                  
| LIDENT+                                                       {List.map (fun id ->(mk_id id $startpos $endpos)) $1}

leg_param:
| LIDENT                                                        { Tident (Qident (mk_id $1 $startpos $endpos) )}
| numeral                                                       { Tconst $1 }
| TRUE                                                          { Ttrue }
| FALSE                                                         { Tfalse }


legend_extend:                                     
| lident_extra leg_param*                                       {(List.hd $1,(List.map (fun id -> Tident (Qident id)) (List.tl $1))@$2) } 


legend_def:
| separated_pair(legend_extend,SEMICOLON,legend_terms)          { let (a,b) = $1 in (Some a, b) }
| legend_terms                                                  { (None, $1) }
| legend_extend                                                 { (Some $1, []) } 


legend_decl:
| attrs(lident_rich) params OF ty  EQUAL legend_def
{    
    let (opt_legendcall,legend_term) = $6 in
    let legendcall_tident = mk_legendcalltermfromident opt_legendcall $startpos $endpos in
    let legend_term_list = mk_term_of_legends legend_term (append_elem $2 (mk_color_type $4  $2)) $startpos $endpos in
    let legend_conjucncts = mk_conjunctlegendterms legend_term_list legendcall_tident $startpos $endpos in
    (* add meta transformation by default to legends *)
    let leg_meta = mk_legend_rewite_rule $1 $startpos $endpos in

        ({ ld_ident = $1; 
        ld_params = append_elem $2 (mk_color_type $4 $2); (* int -> col *)
        ld_type = None;
        ld_def = Some (legend_conjucncts); 
        ld_loc = floc $startpos $endpos }, leg_meta ) } 


type_decl_color:
| attrs(lident_nq) ty_var* typedefn_color invariant* type_witness
  { let (vis, mut), def = $3 in
   (* let algebraic_ty_with_anon = mk_add_anon_to_algebraic_ty $1 def $startpos $endpos in *)
    (* add color types and its construcors to the hashtable *)
    let _ = List.map (fun a -> add_type $1 a ) (return_algebraic_Dtypes $1 def $startpos $endpos)  
    in 
    { td_ident = $1; td_params = $2;
      td_vis = vis; td_mut = mut;
      td_inv = $4; td_wit = $5; td_def = def (* algebraic_ty_with_anon *) ;
      td_loc = floc $startpos $endpos } }

typedefn_color:
| EQUAL vis_mut bar_list1(type_case)
  { $2, TDalgebraic ($3) }

%public 
pure_decl:
| COLOR with_list1(type_decl_color)         { Dtype $2 }
| MAPS coloring_decl with_logic_decl*       { Dlogic ($2::$3) }

%public
legend_embmeta_decl:
| LEGEND legend_decl                        { let (leg,meta) = $2 in [Dlogic [leg];meta] } 

%public 
module_decl:
| d = legend_embmeta_decl
    { 
        List.map ( fun a ->
             Typing.add_decl (floc $startpos $endpos) a;
             add_record_projections a ) d  ;
             ()
    }

coloring_decl:
| attrs(lident_rich) params col_ty preceded(EQUAL,coloring)?
  { 
    let (ty_qident, ty) = $3 in 
    let ty_ident = match ty_qident with (Qident id) -> id | _ -> mk_id_noloc "" in 
    let coloring_def = 
        match $4 with 
            Some col_def -> 
                Some (mk_term_noloc (Tquant (Dterm.DTlambda, 
                [(Loc.dummy_position, Some (mk_id_noloc "i"), false, None )], 
                [], 
                (mk_term_noloc (Tidapp ( coloring_fun_qid, [Lib.Translate.norm_pd_to_why3_dt col_def; mk_term_noloc (mk_Tident (mk_id_noloc "i") )]))))))
            | None -> Some (mk_term_noloc (Tquant (Dterm.DTlambda, [Lib.Ptree_util.fun_binder], [], (mk_term_noloc (Tidapp (Lib.Ptree_util.qid_none,[])))) )) in 
      { ld_ident = $1; ld_params = $2; ld_type = Some ty; 
        ld_def = coloring_def; ld_loc = floc $startpos $endpos } }

col_ty:
| COLON lqualid 
{ ($2,  PTarrow 
        (mk_Ttyapp (mk_id "int" $startpos $endpos), 
        PTtyapp (Qident (mk_id "option" $startpos $endpos) , [PTtyapp ($2,[])] )))  }

%public
meta_arg:
| MAPS qualid  { Mfs $2 }
| LEGEND qualid  { Mps $2 }

