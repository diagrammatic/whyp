open Why3
open Wstdlib
open Ident
open Theory
open Pmodule

let debug = Debug.register_info_flag "print_modules"
  ~desc:"Print@ program@ modules@ after@ typechecking."

let build_parsing_function entry lb = Loc.with_location (entry Whylexer.token) lb

let read_channel env path file c =
  let lb = Lexing.from_channel c in
  Loc.set_file file lb;
  Typing.open_file env path;
  let mm =
    try
      build_parsing_function Whyparser.mlw_file lb
    with
      e -> ignore (Typing.discard_file ()); raise e
  in
  if path = [] && Debug.test_flag debug then begin
    let print_m _ m = Format.eprintf "%a@\n@." print_module m in
    let add_m _ m mm = Mid.add m.mod_theory.th_name m mm in
    Mid.iter print_m (Mstr.fold add_m mm Mid.empty)
  end;
  mm

let register_format = 
  Env.register_format Pmodule.mlw_language "whyp" ["whyp"] read_channel
    ~desc:"WhyML@ programming@ and@ specification@ language"