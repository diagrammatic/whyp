(* This module handles translation of parsed partition diagrams (parsed_pd) into normalized ones (norm_pd) *)

exception NotValid of string

(* val intermediate_translation: Partition.parsed_pd -> Partition.sequence
val intermediate_to_norm: Partition.sequence -> Partition.norm_pd *)

val translate: Partition.parsed_pd -> Partition.norm_pd
val norm_pd_to_why3_dt: Partition.norm_pd -> Why3.Ptree.term
