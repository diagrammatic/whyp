(* Convenience functions for creating PTree.terms *)
open Why3
open Why3.Ptree

(* create term with no-care location *)
let term_noloc term_desc = {
  term_desc = term_desc; 
  term_loc = Loc.dummy_position 
}

(* create ident with no-care location *)
let ident_noloc id_str = {
  id_str = id_str; 
  id_ats = [];
  id_loc = Loc.dummy_position;
}

let tident_noloc qlid = term_noloc (Ptree.Tident (Ptree.Qident qlid))

let tident_desc qlid = Ptree.Tident (Ptree.Qident qlid)

(* option type identifiers*)
let qid_some = Qident (ident_noloc "Some")
let qid_none = Qident (ident_noloc "None")

(* type qid *)
let qid_option = Qident (ident_noloc "option")
let qid_bool = Qident (ident_noloc "bool")
let qid_int = Qident (ident_noloc "int")

(* theory functions and datatypes*)

(* theory prefix *)
let partition_th_qid = Qident (ident_noloc "P")
let coloring_th_qid = Qident (ident_noloc "M")

(* theory datatypes *)
let qid_part_dt = Qdot (partition_th_qid, ident_noloc "Part")
let qid_last_dt = Qdot (partition_th_qid, ident_noloc "Last") 

let qid_c_part_dt = Qdot (coloring_th_qid, ident_noloc "Part")
let qid_c_last_dt = Qdot (coloring_th_qid, ident_noloc "Last") 

(* theory functions *)
let qid_pred = Qident (ident_noloc "pred")
let qid_coloring = Qdot (coloring_th_qid, ident_noloc "mapping")

let var_i = ident_noloc "~i"
let fun_binder = (Loc.dummy_position , Some var_i , false ,Some (PTtyapp (qid_int,[])))

(* combine two terms with infix operator *)
let infix_apply t1 ops t2  =
  let op = ident_noloc (Ident.op_infix ops) in
  term_noloc (Tinfix (t1,op,t2))

(* combine two arithmetic terms with infix arithmetic operator *)
let infix_apply_arith t1 ops t2 = 
  let op = ident_noloc (Ident.op_infix ops) in
  term_noloc (Tidapp ((Qident op),[t1; t2]))

let term_int_one = 
  (term_noloc 
    (Tconst 
      (Number.ConstInt 
        {Number.ic_negative = false; 
        Number.ic_abs = (Number.int_literal_dec "1")})))

let i_binder = 
  (Loc.dummy_position,
  Some var_i,
  false, 
  Some (PTtyapp (Qident (ident_noloc "int"),[])))

let forall_tquant lbinder llterm term = 
  term_noloc (Tquant (Dterm.DTforall, 
    lbinder, 
    llterm ,
    term ))

let tlambda lbinder llterm term =
  term_noloc (Tquant (Dterm.DTlambda, 
    lbinder, 
    llterm,
    term ))

let binop_and t1 t2 = 
  Ptree.Tbinnop (t1 , Dterm.DTand, t2)

let binop_implies t1 t2 = 
  Ptree.Tbinnop (t1 , Dterm.DTimplies, t2)
