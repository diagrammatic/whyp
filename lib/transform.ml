open Why3

let default_transforms: string list = [
  (* TODO: inline_trivial and annotation [@inline:trivial] on generataed predicates / functions may be enough after 
     in next version of Why3 (see https://lists.gforge.inria.fr/pipermail/why3-club/2019-March/001913.html) *)
  "inline_trivial";
  "compute_specified";
  "split_goal_full";
  (* "split_vc"; *)
]

let apply_transforms_on_task_list env task xfms =
  List.fold_left 
    (fun a b -> List.flatten (List.map (Trans.apply_transform b env) a)) 
    task
    xfms 

let apply_transform_on_task env task xfms =
  List.fold_left (fun a b -> List.flatten (List.map (Trans.apply_transform b env) a)) [task] xfms

(* apply the transformation based on the goal, 
  currently there are 2 set of transformations, one for whyml proof obligation, otherwise the default transformations *)
let get_transforms goal =
  if (Str.string_match (Str.regexp ".*VC.*") goal 0) then 
   (* ["split_vc"]@  
    [ "inline_goal"; 
      "split_vc";
      "introduce_premises";
      "inline_goal"] *)
  (*  ["split_all_right";"introduce_premises";"compute_in_goal";"split_all_right"; "eliminate_if";"compute_in_goal"] *)
  ["split_all_right";"compute_in_goal";"introduce_premises";]
  else 
    default_transforms
