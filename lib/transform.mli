(* default transformations applied to all goals *)
val default_transforms: string list

(* apply all default transformations to a list of tasks *)
val apply_transforms_on_task_list : Why3.Env.env -> Why3.Task.task list -> string list -> Why3.Task.task list

val apply_transform_on_task :  Why3.Env.env -> Why3.Task.task -> string list -> Why3.Task.task list 

(* get a list of transformations based on the goal *)
val get_transforms : string  -> string list 