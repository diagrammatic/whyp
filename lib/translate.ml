
open Partition
open Ptree_util

exception NotValid of string

let minus_one t =
  infix_apply_arith t "-" Ptree_util.term_int_one 

let apply_pred t = 
  Ptree_util.term_noloc (Tidapp (Ptree_util.qid_pred, [t]) )

let fill_col oc r =
  match r with 
    | Some _ -> [None; oc]
    | None -> [oc]

let translate_pd_diag pd r =
  match pd with 
  | SeqLR (a,b,oc) -> 
    ([ apply_pred a; b ], (fill_col oc r), Some b)
  | SeqL (a,oc) -> 
    ([apply_pred a],  (fill_col oc r), None) 
  | SeqR (b,oc) -> 
    (match r with 
     | Some(_) -> ([b], [oc], Some b) 
     | None -> raise (NotValid "Missing bound"))        
  | EMPTY_Seq oc -> 
    (match r with
     | Some(_) -> ([], [oc], None)
     | None -> raise (NotValid "Missing bound"))
  | NONEMPTY_CELL (a,oc) -> 
    ([ apply_pred a; a ],  (fill_col oc r), Some a)

let rec translate_pd_diags pds r =
  match pds with
  | [] -> ([],[])
  | pd :: tail -> 
    let p,c,r' = translate_pd_diag pd r in
    let ps,cs = translate_pd_diags tail r' in
    (p @ ps), (c @ cs)

let translate_parsed_pd (first, pd_diags, last) =
  let parts0 = match first with
    | Some t -> [t]
    | None -> [] in
  let parts,cols = translate_pd_diags pd_diags first in
  let parts1 = match last with 
    | Some t ->[apply_pred t]
    | None -> [] in
  parts0 @ parts @ parts1, cols

let rec term_list_to_norm_pd plist clist =
  let c,ctail = match clist with
    | [] -> None,[]
    | c :: ctail -> (c,ctail) 
  in
  match plist with 
  | [] -> raise (NotValid "Missing bound")
  | [t1 ; t2] -> Last (t1,c,t2)
  | p :: ptail -> Part (p, c, term_list_to_norm_pd ptail ctail)

let translate parsed_pd =
  let ps,cs = translate_parsed_pd parsed_pd in
  term_list_to_norm_pd ps cs

let rec norm_pd_to_why3_dt nm_pd =
  match nm_pd with 
  | Part (t1, cl, n_pd) -> 
      (match cl with 
        (Some v) -> term_noloc (Tidapp (qid_c_part_dt, [t1; term_noloc (Tidapp (qid_some, [v])); norm_pd_to_why3_dt n_pd;]))
      | None -> term_noloc (Tidapp (qid_c_part_dt, [t1; term_noloc (Tidapp (qid_none, [])); norm_pd_to_why3_dt n_pd])) )
  | Last (t1,cl,t2) ->  
      (match cl with 
        (Some v) -> term_noloc (Tidapp (qid_c_last_dt, [t1; term_noloc (Tidapp (qid_some, [v])); t2])) 
      | None -> term_noloc (Tidapp (qid_c_last_dt, [t1; term_noloc (Tidapp (qid_none, [])); t2]))) 

