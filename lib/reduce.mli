(* This module handles reduction of normalized partition diagrams into Why3 parsed terms (Why3.Ptree.term) *)

(* val partitionToPtree: Partition.parsed_pd -> Why3.Ptree.term *)

val norm_to_ptree: Partition.norm_pd -> Why3.Ptree.term (* integer inequality term *)

val norm_to_partitionTyped_ptree: Partition.norm_pd -> Why3.Ptree.term (* Partition term *)

val coloringfunctionToPtree: ?qid:Why3.Ptree.binder -> Partition.partColorList -> Why3.Ptree.term

val coloringlegendtoPtree: Partition.partColorList * Why3.Ptree.ident list * Why3.Ptree.ident-> Why3.Ptree.term

val make_coloredpartList: Partition.parsed_pd -> Partition.partColorList

val norm_to_list_terms2:  Partition.norm_pd list -> Partition.partColorList 
