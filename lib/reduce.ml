open Partition
open Why3

exception Not_Valid_Coloring

(*
let rec subtoPtree a = match a with 
  | NONEMPTY_CELL t -> [t]
  | SeqL t -> [t]
  | SeqR t ->  [t]
  | EMPTY_Seq -> []
  | SeqLR (t1,t2) ->  [t1;t2]
  | StartWithTerm (t,sub) -> [t]@(subtoPtree sub)

let endToPtree a = match a with 
  | Some t -> [t]
  | None ->  []

let rec listToPtree lst = match lst with
  | [] -> Ptree_util.term_noloc Ptree.Ttrue
  | a1::[] -> a1
  | a1::l -> Ptree_util.infix_apply a1 "<" (listToPtree l)

let rec partitionToList a = 
  match a with 
  | End (sub,e) -> (subtoPtree sub)@(endToPtree e)
  | Seq2 (sub,diag) -> (subtoPtree sub)@(partitionToList diag)

let partitionToPtree a = 
  let termlist = (partitionToList a) 
  in listToPtree termlist
*)

let term_noloc t = Ptree_util.term_noloc t
let leq_apply a b = Ptree_util.infix_apply a "<=" b
let le_apply a b  = Ptree_util.infix_apply a "<" b
let eq_apply a b  = Ptree_util.infix_apply a "=" b

let mk_unknown_color dt_type_ident = Ptree_util.tident_noloc (Ptree_util.ident_noloc ("~MYCOLOR_"^dt_type_ident))


let vari = Ptree_util.tident_desc (Ptree_util.var_i)

let rec norm_to_ptree norm =
  match norm with 
    | Part (a, _, next) -> leq_apply a (norm_to_ptree next)
    | Last (a, _, b) -> leq_apply a b 

let ty_none_in_partition = 
  term_noloc (Ptree.Tcast 
    (term_noloc 
      (Tidapp (Ptree_util.qid_none, [])), 
        PTtyapp (Ptree_util.qid_option, 
        [PTtyapp (Ptree_util.qid_bool, [])])))
    
let rec norm_to_partitionTyped_ptree norm =
  match norm with 
    | Part (a, _, next) -> 
        term_noloc (Ptree.Tidapp (Ptree_util.qid_c_part_dt, [a; ty_none_in_partition; (norm_to_partitionTyped_ptree next) ]))
    | Last (a, _, b) -> 
        term_noloc (Tidapp (Ptree_util.qid_c_last_dt, [a;ty_none_in_partition;b])) 


let ineq_part_binder b t = leq_apply (term_noloc (Ptree_util.tident_desc b)) t

(* Convert the normalized partition to a list of tuple
  first element of the tuple, partition predicate with i binder,
  second element is the specified color for the part *)
let rec norm_to_list_ineq ?(bind=Ptree_util.var_i) norm =
  match norm with 
  | Part (a, cl, next) -> 
      (match next with
         Part (b, _, _) -> (le_apply a (ineq_part_binder bind b),cl)::(norm_to_list_ineq next) 
       | Last (b, _, _) -> (le_apply a (ineq_part_binder bind b),cl)::(norm_to_list_ineq next))
  | Last (a, cl, b) -> [(le_apply a (ineq_part_binder bind b),cl)]

(* Three tupled list *)
let rec norm_to_list_terms norm =
  match norm with 
  | Part (a, cl, next) -> 
      (match next with
         Part (b, _, _) -> (a,b,cl)::(norm_to_list_terms next) 
       | Last (b, _, _) -> (a,b,cl)::(norm_to_list_terms next))
  | Last (a, cl, b) -> [(a,b,cl)]

let is_Some (part,cl) = 
  match cl with 
    Some _ -> true 
    | _ -> false 

(* Filter out the un-colored parts *)
let normed_coloredSpecified_list ltuple = List.filter (fun a -> is_Some a) ltuple  

(* replace the option to the actual value for color in the (part,color) tuple *)
let colored_specified_getOpt ltuple = 
  List.map 
    (fun (part,cl) -> 
      (part, Opt.get cl)) 
    (normed_coloredSpecified_list ltuple)

let makeIfElseFromTuple normed_color = 
  List.fold_right 
    (fun (a,cl) b -> 
      term_noloc 
        (Ptree.Tif (a,term_noloc (Ptree.Tidapp (Ptree_util.qid_some, [cl])), 
          b)))
     normed_color (term_noloc (Ptree.Tident Ptree_util.qid_none))

(* Three tupled term list to two tuple (ineq term, return value term) *)
let list_to_list ?(bind=Ptree_util.var_i) l = List.map (fun (a,b,c) -> (le_apply a (ineq_part_binder bind b),c)) l

let coloringfunctionToPtree ?(qid = Ptree_util.fun_binder) (coloring_def) = 
  let (a,b,c,d) = qid in 
    let coloring_bind = colored_specified_getOpt (list_to_list coloring_def ~bind:(Opt.get b)) in 
    if (List.length coloring_bind > 0) then 
      Ptree_util.tlambda [qid] [] (makeIfElseFromTuple coloring_bind)
    else 
      term_noloc (Ptree.Tquant (Dterm.DTlambda, [qid], [], term_noloc (Ptree.Tident Ptree_util.qid_none))) 
  

let make_quantified_term color_term var_ids cl_id cl_val =  
  let apply_term_from_ident_list =
    List.fold_left 
      (fun v1 v2 -> 
        Ptree.Tapply (term_noloc v1,v2)) 
        (Ptree_util.tident_desc cl_id) [term_noloc vari] in

  let quantified_term = 
    term_noloc 
      (Ptree_util.binop_implies color_term (eq_apply (term_noloc apply_term_from_ident_list) cl_val)) in
      Ptree_util.forall_tquant [Ptree_util.i_binder] [] quantified_term 

let rec coloringlegendtoPtree (legend_color_pred,state_var_ids,cl_id) = 
  let quant_color_conjunct = 
    List.fold_right 
      (fun a b ->
        let (part,cl) = a in 
        let quant_term = make_quantified_term part state_var_ids cl_id (term_noloc (Ptree.Tapply (term_noloc (Ptree.Tident Ptree_util.qid_some),cl))) in  
        Ptree_util.binop_and quant_term (term_noloc b))
      (colored_specified_getOpt (list_to_list legend_color_pred ))
      Ptree.Ttrue in
      term_noloc quant_color_conjunct

(* List of tuple (part term,color) *)
let make_coloredpartList pars_pd =
  norm_to_list_terms (Translate.translate pars_pd)

  let norm_to_list_terms2 normed_pd_list =
    List.flatten (List.map (fun i -> norm_to_list_terms i) normed_pd_list)
