open Why3

type parsed_pd_diag =
  | EMPTY_Seq of Ptree.term option
  | NONEMPTY_CELL of Ptree.term * Ptree.term option
  | SeqL of Ptree.term * Ptree.term option
  | SeqR of Ptree.term * Ptree.term option
  | SeqLR of Ptree.term * Ptree.term * Ptree.term option

(* This type represents a parsed partition diagram, which may contain syntactic shorthands such as "|a|" and "a|...|a" *)
type parsed_pd = Ptree.term option * parsed_pd_diag list * Ptree.term option 

(* This type represents a partition diagram in normalized form, with optional coloring *)
type norm_pd =
  | Part of Ptree.term * Ptree.term option * norm_pd
  | Last of Ptree.term * Ptree.term option * Ptree.term

type sequence = Seq of parsed_pd_diag | SeqRec of parsed_pd_diag * sequence

type partColor = (Ptree.term * Ptree.term) list 
type partColorList = (Ptree.term * Ptree.term * Ptree.term option) list 
(* Left Side Part's term, Right Side Part's term, returned term: ex: color in colorig *)

