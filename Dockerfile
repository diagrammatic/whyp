FROM ocaml/opam2:debian-stable
ARG build_packages="autoconf m4 libz-dev libgmp3-dev"
ARG z3="z3-4.8.4.d6df51951f4c-x64-debian-8.11"
ARG cvc4="cvc4-1.6-x86_64-linux-opt"

RUN sudo apt-get update && \
    sudo apt-get install -yq ${build_packages} && \
    opam repository add opam https://opam.ocaml.org --set-default && \
    opam repository remove default --all && \
    opam install merlin ocp-indent dune utop ounit core && \
    opam install why3.1.2.0 alt-ergo.2.3.0 && \
    opam clean && \
    curl -L -O https://github.com/Z3Prover/z3/releases/download/z3-4.8.4/${z3}.zip && \
    unzip ${z3}.zip && \
    sudo cp -rp ${z3}/bin/* /usr/local/bin && \
    rm -rf ${z3}.zip ${z3} && \
    curl -L -O http://cvc4.cs.stanford.edu/downloads/builds/x86_64-linux-opt/${cvc4} && \
    sudo cp -rp ${cvc4} /usr/local/bin/cvc4 && \
    sudo chmod ugo+rx /usr/local/bin/cvc4 && \
    sudo apt-get remove --purge -y $BUILD_PACKAGES && \
    sudo rm -rf /var/lib/apt/lists/*
